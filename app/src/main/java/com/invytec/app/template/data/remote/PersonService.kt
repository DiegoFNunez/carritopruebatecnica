package com.invytec.app.template.data.remote

import com.invytec.app.template.data.entities.ResponseProducts
import retrofit2.Response
import retrofit2.http.GET

//TODO:
interface PersonService {

    @GET("movie/popular?api_key=b74b939ca6bca5d66e703d8a9b837575&language=en-US&page=1")
    suspend fun getAllProducts(): Response<ResponseProducts>
}
