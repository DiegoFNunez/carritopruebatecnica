package com.invytec.app.template.common

import androidx.appcompat.app.AppCompatActivity

open class BaseActivity : AppCompatActivity()
