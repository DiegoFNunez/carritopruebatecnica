package com.invytec.app.template.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.invytec.app.template.R
import com.invytec.app.template.data.entities.Product
import kotlinx.android.synthetic.main.item_row.view.*

class RecyclerProductsAdapter(private val context: Context, val listener: OnProductClickListener) : RecyclerView.Adapter<RecyclerProductsAdapter.CardCouponHolder>() {

    interface OnProductClickListener {
        fun onClickProduct(product: Product)
    }

    private var dataList = listOf<Product>()

    fun setListData(data:List<Product>){
        dataList = data
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CardCouponHolder {

        //TODO:
        val view = LayoutInflater.from(context).inflate(R.layout.item_row,parent,false)
        return CardCouponHolder(view)

    }

    override fun getItemCount(): Int {
        return if(dataList.size > 0){
            dataList.size
        }else{
            0
        }
    }

    override fun onBindViewHolder(holder: CardCouponHolder, position: Int) {
        val user = dataList[position]
        holder.bindView(user)
    }

   inner class CardCouponHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

       fun bindView(product:Product){

           //TODO:The TMDb API is returns null image url
           //Glide.with(context).load(product.).into(itemView.circleImageView)

           itemView?.textViewTitle.text = product.title
           itemView?.textViewOverview.text = product.overview

           itemView.setOnClickListener {

               listener.onClickProduct(product)
           }
       }
    }
}
