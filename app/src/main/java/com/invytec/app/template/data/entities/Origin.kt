package com.invytec.app.template.data.entities

data class Origin(
    val name: String,
    val url: String
)
