package com.invytec.app.template.data.repository

import androidx.lifecycle.LiveData
import com.invytec.app.template.data.entities.Product
import com.invytec.app.template.data.local.ProductDao
import com.invytec.app.template.data.remote.PersonService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.Exception
import javax.inject.Inject

class PersonRepository @Inject constructor(
    private val service: PersonService,
    private val dao: ProductDao
) {

    suspend fun getProducts(): List<Product>? {

        //DB
        try {
            val local = withContext(Dispatchers.IO) { dao.getAllCharacters() }
            if ((local != null) && (local.isNotEmpty())) {
                return local
            }
        } catch (exception: Exception) {
            Timber.e(exception)
            return null
        }

        //API
        try {
            val response = service.getAllProducts()
            return response.body()?.results?.also {
                dao.insertAll(it)
            }

        } catch (exception: Exception) {
            Timber.e(exception)
            return null
        }

    }
}
