package com.invytec.app.template.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController

import com.invytec.app.template.R
import com.invytec.app.template.common.BaseFragment
import com.invytec.app.template.data.entities.Product
import com.invytec.app.template.data.entities.Usuario
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_product_list.*
import javax.inject.Inject



/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
@AndroidEntryPoint
class ProductListFragment : BaseFragment(), RecyclerProductsAdapter.OnProductClickListener {

    lateinit var modelSharedViewModel: SharedViewModel

    private var mContext: Context? = null

    private lateinit var mAdapter: RecyclerProductsAdapter

    @Inject
    lateinit var viewModel: ProductListViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_product_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.button_go_third_fragment).setOnClickListener {

            findNavController().navigate(R.id.action_FirstFragment_to_ThirdFragment)

        }

        //TODO:
        mAdapter = mContext?.let { RecyclerProductsAdapter(it, this) }!!

        rvCoupons?.adapter = mAdapter

        //MVVM
        observeData()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    fun observeData(){

        shimmer_view_container.startShimmer()
        viewModel.productsLiveData.observe(viewLifecycleOwner, Observer{
            shimmer_view_container.stopShimmer()
            shimmer_view_container.hideShimmer()
            shimmer_view_container.visibility = View.GONE
            mAdapter.setListData(it)
            mAdapter.notifyDataSetChanged()
        })
    }

    override fun onClickProduct(producto: Product) {

        modelSharedViewModel = ViewModelProvider(requireActivity()).get(SharedViewModel::class.java)
        modelSharedViewModel.sendMessage("MindOrksWith Product")
        modelSharedViewModel.sendProduct(producto)
        findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)

    }
}
