package com.invytec.app.template.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.invytec.app.template.common.BaseViewModel
import com.invytec.app.template.data.entities.Product
import com.invytec.app.template.data.entities.Usuario
import com.invytec.app.template.data.repository.PersonRepository
import com.invytec.app.template.data.repository.ProductListRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class ProductListViewModel @Inject constructor(repository: PersonRepository) : BaseViewModel() {

    val productsLiveData: MutableLiveData<List<Product>> = MutableLiveData()

    init {
        viewModelScope.launch {
            repository.getProducts()?.let {
                productsLiveData.postValue(it)
            }
        }
    }
}
