package com.invytec.app.template.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.invytec.app.template.data.entities.Product

@Dao
interface ProductDao {

    @Query("SELECT * FROM product")
    fun getAllCharacters(): List<Product>

    @Query("SELECT * FROM product WHERE id = :id")
    fun getCharacter(id: Int): Product?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(product: List<Product>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(product: Product)
}
