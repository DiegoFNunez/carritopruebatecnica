package com.invytec.app.template.data.entities

data class Info(
    val count: Int,
    val next: String,
    val pages: Int,
    val prev: Any
)
