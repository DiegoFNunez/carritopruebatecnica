package com.invytec.app.template.data.entities

data class Location(
    val name: String,
    val url: String
)
