package com.invytec.app.template.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.invytec.app.template.data.entities.Product

class SharedViewModel: ViewModel() {
    val message = MutableLiveData<String>()
    //TODO:
    var product = MutableLiveData<Product>()

    fun sendMessage(text: String) {
        message.value = text
    }
    fun sendProduct(product: Product) {
        this.product.value = product
    }
}