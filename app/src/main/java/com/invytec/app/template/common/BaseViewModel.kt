package com.invytec.app.template.common

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()
