package com.invytec.app.template.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.invytec.app.template.data.local.AppDatabase
import com.invytec.app.template.data.local.ProductDao
import com.invytec.app.template.data.remote.PersonService
import com.invytec.app.template.data.repository.PersonRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun providePersonService(retrofit: Retrofit): PersonService = retrofit.create(PersonService::class.java)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) = AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun providePersonDao(db: AppDatabase) = db.characterDao()

    @Singleton
    @Provides
    fun provideRepository(characterService: PersonService, dao: ProductDao) = PersonRepository(characterService, dao)
}
