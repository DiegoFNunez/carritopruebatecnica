package com.invytec.app.template.data.entities

data class ResponseProducts(

    val page: Int,
    val results: List<Product>,
    val total_pages: Int,
    val total_results: Int
)